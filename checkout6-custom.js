var Medialab = function(a, b, c) {
    var body = $('body');
    var getId = null;

    Coliseum = function() {
            fn.Ready();
        },
        fn = {
            Ready: function() {
                fn.documentAjax();
                fn.documentOnload();
                fn.documentReady();
            },
            documentAjax: function() {
                $(document).ajaxStop(function() {
                    setTimeout(function(){                         
                        fn.mensajeCov19();
                        fn.pagoEfectivo();
                        fn.pagoAgencias();
                    }, 1000);                    
                });
                $(window).on('orderFormUpdated.vtex', function(evt, orderForm){
                    console.log(orderForm);
                });
                $(window).on('hashchange', function(e) {
                    if (location.hash === "#/payment") {
                    }
                    if (location.hash === "#/shipping") {
                        fn.mensajeCov19();                        
                    }
                    if (location.hash === "#/profile") {
                        $("#client-document").attr('maxlength','8')
                        $("#client-document").attr('minlength','8')
                        $("#client-document").attr('pattern','[0-9]*')
                        $("#client-company-document").attr('maxlength','11')
                        $("#client-company-document").attr('pattern','[0-9]*') 
                    }
                });
            },
            documentOnload: function() {
                $(window).on('load', function() { //Al final, lee cuando ya cargo todo
                    //setTimeout(function(){ fn.showRegalo();}, 500); 
                    //setTimeout(fn.pagoefectivo(),1000);
                });
            },
            documentReady: function() { //Al iniciar, lee conforme va cargando
                //fn.mercadoPago();
                fn.insertMetaScritpHead();
                setTimeout(function(){ 
                    fn.pagoSatisfactorio()
                }, 2000);
            },
            showRegalo: function() {
                //body.removeClass('readyRegalo');
                $('.cart-template  .cart-template-holder .cart .table.cart-items').after('<p class="mensaje-ahorra-mas" style="font-size: 14px;color: #002ea4 !important;border: 0;margin: 5% 0 0 0;background: #fbfbfb;text-align: center;padding: 17px 0;"><img src="https://coliseum.vteximg.com.br/arquivos/logo-cyber-checkout.png"/>&nbsp;&nbsp; <b>Obtén 20% DSCTO. adicional</b> en productos seleccionados con el cupón INTERBANK <br>El descuento se verá reflejado al ingresar tu tarjeta Interbank</p>');
                //$('.cart-template  .cart-template-holder .cart .table.cart-items').after('<p class="mensaje-ahorra-mas" style="font-size:14px;color:#000">ENVÍO GRATIS POR COMPRAS DESDE S/200</p>');
            },
            mensajeCov19: function(){
              var city01 = ($('.address-summary.address-summary-PER .city').html()) ? $('.address-summary.address-summary-PER .city').html().toLowerCase() : '';
              var city02 = ($('#ship-city').val()) ? $('#ship-city').val().toLowerCase() : '';
              var recojotienda = $('.vtex-omnishipping-1-x-SummaryItemTitle').html();

              if(recojotienda === 'Recoger en la tienda') {
                //$('.vtex-omnishipping-1-x-deliveryGroup .shp-option-text-label-single.vtex-omnishipping-1-x-leanShippingTextLabelSingle span').html('Programado');
                //$('.box-step.box-info.shipping-summary-placeholder .false.shp-summary-package-time span').html('Programado');
                //$('.checkout-container .cart-fixed .summary-cart-template-holder .cart .item .description .shipping-date').html('Programado');
              } else if(city01.includes('lim') || city02.includes('lim') || city01.includes('cal') || city02.includes('cal')){
                //$('.vtex-omnishipping-1-x-deliveryGroup .shp-option-text-label-single.vtex-omnishipping-1-x-leanShippingTextLabelSingle span').html('Envío desde 1 hasta 5 días hábiles.');
                //$('.box-step.box-info.shipping-summary-placeholder .false.shp-summary-package-time span').html('Envío desde 1 hasta 5 días hábiles.');
                //$('.checkout-container .cart-fixed .summary-cart-template-holder .cart .item .description .shipping-date').html('Envío desde 1 hasta 5 días hábiles.');
              } else {
                //$('.vtex-omnishipping-1-x-deliveryGroup .shp-option-text-label-single.vtex-omnishipping-1-x-leanShippingTextLabelSingle span').html('En hasta 10 días hábiles');
                //$('.box-step.box-info.shipping-summary-placeholder .false.shp-summary-package-time span').html('En hasta 10 días hábiles');
              }
            },            
            mercadoPago: function() {
                var script = document. createElement ( "script" );
                script. src = " https://www.mercadopago.com/v2/security.js " ;
                script. setAttribute ( "output" , "vtex.deviceFingerprint" );
                script. setAttribute ( "view" , "checkout" );
                document.body. appendChild (script);
            },
            insertMetaScritpHead: function() {
                var script_head01 = document.createElement('meta');
                script_head01.name = "robots";
                script_head01.content = "noindex,nofollow";
                document.getElementsByTagName('head')[0].appendChild(script_head01);
            },
            pagoEfectivo: function() {
                $('.PagoEfectivoPaymentGroup .payment-description').html('<br><p class="text-pago-efectivo"><b>Depósitos en efectivo vía PagoEfectivo - </b> Paga en BBVA, BCP, Interbank, Scotiabank, BanBif, Western Union, Tambo+, Kasnet, Full Carga, Red Digital, Money Gram, Caja Arequipa, Disashop, en cualquier agente o agencia autorizada a nivel nacional a la cuenta de PagoEfectivo</p><p class="text-pago-efectivo"><b>Transferencias bancarias vía PagoEfectivo - </b> Paga en BBVA, BCP, Interbank, Scotiabank, BanBif, Caja Arequipa, a través de la banca por internet o banca móvil en la opción pago de servicios </p><br><p class="text-pago-efectivo"><b>Recuerda que tienes 12 horas para realizar el pago</b></p><br>')
            },  
            pagoAgencias: function() {
                $('.payment-method .custom202PaymentGroupPaymentGroup').html('<img src="/arquivos/pagoAgencias2.jpg"/>')
            },             
            pagoSatisfactorio: function() {
                var ordernumber =  $('.vtex-order-placed-2-x-orderNumber').innerHTML;
                $('.vtex-order-placed-2-x-confirmationMessage').append(' '+ordernumber);

                var pagoEfectivo = $('.vtex-order-placed-2-x-paymentGroup').innerHTML;
                if(pagoEfectivo == 'PagoEfectivo') {
                    $('.vtex-order-placed-2-x-confirmationTitle').innerHTML = 'Pedido Pendiente de Pago';
                }
            }                                 
        }
    Coliseum();
}(jQuery, window);


// SELECTOR CUSTOM DOCUMENTO DE IDENTIDAD
var docTypeSelector = function () {
    // Creamos el contenedor de tipo 'select' y le agregamos un 'id' para poder acceder a el de manera más facil
    const $docTypeSelect = $('<select>').attr('id', 'client-doc-type');

    // Constante del 'input' original
    const $originaldocTypSelect = $('#client-document-type');

    // Opciones que iran dentro del 'select'
    const $docTypeOptions = `
      <option value="dni">DNI</option>
      <option value="ce">Carné de extranjería</option>
      <option value="ps">Pasaporte</option>`;

    // Agregamos las 'option' al 'select'
    $($docTypeOptions).appendTo($docTypeSelect);

    // Agregamos el 'select' antes del input
    $originaldocTypSelect.before($docTypeSelect);
    $('.client-document-type').insertBefore('.client-document');
    // $('.other-document').insertBefore('.client-document');

    // Aca determinamos si antes ya hay algun valor guardado en el 'input', para cambiar el valor que trae por defecto nuestro 'select'
    if ($originaldocTypSelect.val().length > 0) {
        $docTypeSelect.find('option').removeAttr('selected');

        // buscamos la opcion que tenga el 'input' original para agregar el atributo 'selected' y nos lo muestre por defecto
        $docTypeSelect
            .find('option[value="' + $originaldocTypSelect.val() + '"]')
            .prop('selected', 'selected');
    }

    var checkDocTypeExist = function () {
        // actualiza el valor del del 'input' original con el valor seleccionado en el selectv
        var documentType = $('#client-doc-type').val();
        $('#client-document-type').val(documentType);

        if (documentType !== 'dni') {
            clientProfileData.alternateDocumentType(documentType);
            clientProfileData.hasDifferentDocument(true);
        } else {
            clientProfileData.toggleDifferentDocument();
            clientProfileData.document.validate();
        }
    };

    // Evento que escucha cuando nuestro 'select', cambia de valor.
    $(document).on('change', '#client-doc-type', function (e) {
        checkDocTypeExist();
    });

    // Evento que escucha al dar click al seleccionar otro documento, para se disparen los eventos de nuestro 'select'
    $(document).on('click', '.links-other-document.links', function () {
        $('#client-doc-type').trigger('change');
    });

    $(document).on('change', '#client-document2', function (e) {
        if ($('#client-doc-type').val !== 'dni') {
            clientProfileData.alternateDocument(e.target.value);
        }
    });
};

$(document).ready(function () {
    docTypeSelector();
    $('#client-document2').removeAttr('disabled');
});
// FINAL SELECTOR CUSTOM DOCUMENTO DE IDENTIDAD


// ----------------------------< Code>---------------------------- //
// Just update client_id provided by acuotaz
const clientId='se_merrell_pos_pe';
// Do not edit this section
const waitForElement = async element => {
    while (document.querySelector(element) === null)
        await new Promise(resolve => requestAnimationFrame(resolve));
    return document.querySelector(element);
};
['hashchange','load'].forEach( evt =>
    window.addEventListener(evt, function(){
        Promise.all([
            fetch(`https://apurata.com/pos/${clientId}/info-steps`),
            fetch(`https://apurata.com/pos/client/${clientId}/landing_config_open`),
            waitForElement('#payment-group-aCuotazPaymentGroup'),
            waitForElement('.aCuotazPaymentGroup'),
            waitForElement('.aCuotazPaymentGroup span')
    ]).then(async([mock, lim, aCuotazID, aCuotazCls,span]) => {
        const mocker = await mock.text();
        const limits = await lim.json();
        return {mocker, limits,aCuotazID,aCuotazCls,span}
    }).then((r) => {
        // Get total cart amount
        const total = parseFloat(r.span.innerText.slice(r.span.innerText.search('S/') + 3).replace(',', ''))
        const acuotaz_addon = document.getElementById('pay-with-apurata');
        // Check if the total can be financed by aCuotaz
        if (total < r.limits.min_amount || total > r.limits.max_amount) {
            r.aCuotazID.style.display = 'none';
            r.aCuotazCls.style.display = 'none';
        } else {
            r.aCuotazCls.innerHTML = r.mocker;
        }
        // Add add-on once
        if (acuotaz_addon == null){
            fetch(`https://apurata.com/pos/pay-with-apurata-add-on/${total}?page=cart`,{headers: {"Client_id": clientId}},)
            .then((r) => r.text().then(
                function(text) {
                    document.getElementsByClassName('summary-totalizers')[0].insertAdjacentHTML('afterend', text);
                })
            )
        }
        console.log(r, total);
        }).catch((err) => {console.log(err);});
    })
);
// ----------------------------</ aCuotaz Code>---------------------------- //
                           
                           
// --------------------------  OpenPay --------------------------- //
var head = document.getElementsByTagName('head')[0];

var scriptOpenpay = document.createElement('script');

var country ='PE';

console.log(country);

var baseUrl = getScriptBaseUrlByCountry(country);

scriptOpenpay.type = 'text/javascript';

scriptOpenpay.onload = function() {

loadScriptData();

}

 

scriptOpenpay.src = baseUrl + "/openpay.v1.min.js";

head.appendChild(scriptOpenpay);

 

function loadScriptData() {

var scriptData = document.createElement("script");

scriptData.type="text/javascript";

scriptData.src = baseUrl + "/openpay-data.v1.min.js";

scriptData.onload = function() {

generateDeviceSession();

}

head.appendChild(scriptData);

}

 

function generateDeviceSession() {

var sandboxMode = ('true' === 'true');

OpenPay.setId('mzysqc9cbdxjtubgtoi2');

OpenPay.setApiKey('pk_2d6839bf54a346a9950d614f46c6671a');

OpenPay.setSandboxMode(sandboxMode);

var deviceSessionIdOpenPay = OpenPay.deviceData.setup();

console.log('deviceSessionIdOpenPay', deviceSessionIdOpenPay);

window.vtex.deviceFingerprint = deviceSessionIdOpenPay;

}

 

function getScriptBaseUrlByCountry(countryCode) {

var scriptBaseUrl = [

{ country: 'MX', baseUrl: 'https://openpay.s3.amazonaws.com' },

{ country: 'CO', baseUrl: 'https://resources.openpay.co' },

{ country: 'PE', baseUrl: 'https://js.openpay.pe' }

];

return scriptBaseUrl.find(function(item) { return countryCode == item.country }).baseUrl;

}
// ------------------------------------ //                           