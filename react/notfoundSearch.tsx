import React from 'react';

const notfoundSearch = ({children}: any) => {
    const buscavazia = window.location.pathname
      .replace('/','')
      .replace('?_q=','')
      .replace('&map=ft','')
      .replace('?ft=', '');
    console.log('aaa: '+buscavazia);
  
    if (buscavazia.indexOf('buscavazia') === -1) window.history.pushState('','', `/sistema/buscavazia?ft=${buscavazia}`)
    return <>
      {/* <p>No hay resultados con: {buscavazia}</p> */}
      {children}
    </>
  }

export default notfoundSearch;