import { FC, useState } from 'react'
import { useCssHandles } from 'vtex.css-handles'
import { FormattedPrice } from 'vtex.formatted-price'
import { CloseButton, Modal, ModalHeader, ModalTrigger } from 'vtex.modal-layout'
import { usePixelEventCallback } from 'vtex.pixel-manager'
import { Link, useRuntime } from 'vtex.render-runtime'
import { Image } from 'vtex.store-image'
import "./addToCart.css"
import { Sku } from './types/product'
import { ProductContextProvider } from 'vtex.product-context'

interface ModalProductAddProps {
  RelatedProductsShelf: FC
  RelatedProductsShelfMobile: FC
}

const CSS_HANDLES = ['modal-product-add-container', 'modal-product-add-list', 'modal-add-to-cart', 'modal-add-to-cart-close', 'modal-product-add-paragraph', 'modal-product-add-header', 'modal-add-to-cart-paragraph']

const ModalProductAdd: FC<ModalProductAddProps> = ({ RelatedProductsShelf, RelatedProductsShelfMobile }) => {

  const [sku, setSku] = useState<Sku>()
  const { deviceInfo: { isMobile } } = useRuntime()

  const { handles } = useCssHandles(CSS_HANDLES)

  usePixelEventCallback({
    eventName: "addToCart",
    handler: (e: any) => {
      setSku(e?.data.items[0])
    }
  })

  return (
    <ModalTrigger customPixelEventName="addToCart">
      <Modal>
        {sku && (
          <>
            <div className={handles['modal-product-add-header']}>
              <ModalHeader />
            </div>
            <div className={handles['modal-product-add-container']}>
              <span className={handles['modal-product-add-paragraph']}>El producto se agregó al carrito de compras</span>
              <div className={handles['modal-product-add-list']}>
                <Image src={sku.imageUrl} alt={sku.name} />
                <div>
                  <p>{sku.brand}</p>
                  <div>
                    <h2>{sku.name}</h2>
                    <br/>
                    <span>CANTIDAD: {sku.quantity}</span>
                  </div>
                  <div>
                    <span>SKU:{sku.skuId}</span>
                  </div>
                  <FormattedPrice value={sku.price / 100} />
                </div>
              </div>

              <div className={handles['modal-add-to-cart']}>
                <h3 className={handles['modal-add-to-cart-paragraph']}>Complementa tu compra:</h3>
                {/* @ts-ignore */}
                <ProductContextProvider product={{productId: sku.productId}} query={sku.skuId}>
                  {isMobile ? <RelatedProductsShelfMobile /> : <RelatedProductsShelf />}
                </ProductContextProvider>
              </div>

              <div className={handles['modal-add-to-cart-close']}>
                <CloseButton label='Seguir comprando' />
                <div>
                  <Link to="/checkout/#/cart" target='_blank'>Finalizar compra</Link>
                </div>
              </div>
            </div>
          </>
        )}
      </Modal>
    </ModalTrigger>
  )
}

export default ModalProductAdd
