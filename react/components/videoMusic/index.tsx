import style from './styles.css'

const videoMusic = () => {
  return <>
            <div>
				<video controls muted playsInline={true} autoPlay={true} className={`${style.videoCapo}`}>
				       <source src='https://novedadescoliseum.com.pe/devs/archivos/Merrell-music-exprience.mp4' type="video/mp4" />
				</video>		        
            </div>
        </>
}

export default videoMusic;