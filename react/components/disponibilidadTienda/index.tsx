import React, { useState , useEffect} from 'react'
import style from './styles.css'
import {useProduct} from 'vtex.product-context'

const disponibilidadTienda = () => {  
  const [show01, setShow01] = useState(false);
  const [show02, setShow02] = useState(false);

  const getDisponibilidadDespacho  = async () =>{ 
    const {product} = useProduct()      
    var xmlhttp = new XMLHttpRequest();              
    var theUrl = "https://novedadescoliseum.com.pe/devs/apis/disponibilidadTiendas/index-merrell.php";
    xmlhttp.open("POST", theUrl, true);    
    xmlhttp.withCredentials = true;
    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xmlhttp.send('sku='+product?.productId+'&filtro=Despacho');

    xmlhttp.onload = function () {
        if (this.status >= 200 && this.status < 300) {
          document.querySelectorAll('.merrellpe-store-theme-2-x-popupdespacho')[0].innerHTML = xmlhttp.response;                
        }
    }; 
  }
  getDisponibilidadDespacho();

	const getDisponiblitySKU  = async () =>{ 
	    const {product} = useProduct()      
	    var xmlhttp = new XMLHttpRequest();              
	    var theUrl = "https://novedadescoliseum.com.pe/devs/apis/disponibilidadTiendas/index-merrell.php";
	    xmlhttp.open("POST", theUrl, true);    
	    xmlhttp.withCredentials = true;
	    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	    xmlhttp.send('sku='+product?.productId);

	    xmlhttp.onload = function () {
	        if (this.status >= 200 && this.status < 300) {
	        	document.querySelectorAll('.merrellpe-store-theme-2-x-popupretiro')[0].innerHTML = xmlhttp.response;         		            
	        }
	    };             
	}
	getDisponiblitySKU();

  useEffect(()=>{
    var functionClickDisponibilidad = function() { setShow01(true) };
 	  var disponibility = document?.querySelectorAll(".vtex-rich-text-0-x-paragraph--texto-derecha-disponibilidad")[0];
    disponibility.addEventListener('click', functionClickDisponibilidad, false);    

    var functionClickDespacho = function() { setShow02(true) };
    var despacho = document?.querySelectorAll(".vtex-rich-text-0-x-paragraph--texto-derecha-despacho")[0];
    despacho.addEventListener('click', functionClickDespacho, false); 
  }, [])

    const handleModalClose = () => {
    setShow01(false)
    setShow02(false)
    }

  return (
      <div>
        <div className={`${style.popupcontent} ${!show01 ? style.noshow : ''} z-9999` }>
          <div className={`${style.popupcontainerretiro}`} >
          <div className={`${style.popupclose}`}  onClick={handleModalClose}><img src="/arquivos/popupclose.png"/></div>
            <h2 className={`${style.popupagetitle}`}>¡DISPONIBILIDAD PARA RECOJO EN TIENDA! 👋 🏪</h2>
            <br/>
            <p className={`${style.popupretiro}`}></p>
          </div>
        </div>

        <div className={`${style.popupcontent} ${!show02 ? style.noshow : ''} z-9999` }>
          <div className={`${style.popupcontainerdespacho}`} >
          <div className={`${style.popupclose}`}  onClick={handleModalClose}><img src="/arquivos/popupclose.png"/></div>
            <h2 className={`${style.popupagetitle}`}>¡DESPACHO A DOMICILIO! 👋 🏪</h2>
            <br/>
            <p className={`${style.popupdespacho}`}></p>
          </div>
        </div> 
      </div> 
  )
       
}

export default disponibilidadTienda;