import style from './styles.css'
import React, { useEffect, useState } from 'react'
import { ExtensionPoint } from 'vtex.render-runtime'
import { Modal } from 'vtex.styleguide'

export const NEWSLETTER_MODAL_LOCALSTORAGE_KEY = 'modal-newsletter'

export default function CustomModalNewsletter() {
  const [isOpen, setOpen] = useState(false)

  useEffect(() => {
    const localstorageKey = localStorage.getItem(
      NEWSLETTER_MODAL_LOCALSTORAGE_KEY
    )

    if (!localstorageKey) {
      setTimeout(() => {
        setOpen(true)
      }, 2000)
    }
  }, [])

  function onClose() {
    localStorage.setItem(NEWSLETTER_MODAL_LOCALSTORAGE_KEY, 'true')
    setOpen(false)
  }
  console.log(Modal);

  return (
    <Modal
      isOpen={isOpen}
      showCloseIcon={false}
      closeOnEsc={false}         
      centered
      /* onClose={() => onClose()} */
    >
      <div className="flex w-100">
        <ExtensionPoint id="my-other-block" />
        <div className={style.contenPopup}>Este sitio web usa cookies para mejorar tu experiencia. Al navegar en nuestro sitio web, usted acepta todas las cookies de acuerdo con nuestra <a className={style.urlCookies} href="/politica-de-cookies">Política de cookies</a> y <a className={style.urlCookies} href="/politica-de-privacidad">Política de Privacidad</a>.</div>
        <div onClick={() => onClose()} className="flex items-center">
          <div onClick={() => onClose()} className={`${style.closePopup} mr4 ml4`}>CERRAR</div>
        </div>
      </div>
    </Modal>
  )
}