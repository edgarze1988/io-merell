import React from 'react'
import { useCssHandles } from 'vtex.css-handles'

const CSS_HANDLES = ['notfoundtitle', 'background', 'text', 'item'] as const

const TerminoBusqueda = () => {
  const {handles} = useCssHandles(CSS_HANDLES);

  const buscavazia = window.location.search
    .replace('?_q=','')
    .replace('&map=ft','')
    .replace('?ft=', '');

  console.log("buscavazia: "+buscavazia); 

  return <>
            <div className={handles.notfoundtitle}>No hay datos para esta búsqueda***: {buscavazia}</div>
            <div className={`${handles.background} big`}>Intenta de nuevo o puedes regresar Merrell</div>
            <a className="link--notfound-button" href="/">Regresar a Merrell</a>
        </>
}

export default TerminoBusqueda;
//crear un css con este nombre: merrellpe.store-theme-css